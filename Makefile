# TODO: Filter out duplicate MP3s in m3u
# Generate M3U with song titles, lengths

default:
	echo "No default Make target"

.PHONY: \
	default \
	update-website \
	publish-website \
	publish-m3u \
	publish \
	cache-music-files \
	publish-mp3-files

.DELETE_ON_ERROR:

#music.m3u: music.txt txt2m3u
#	./txt2m3u "$<" > "$@"

#music.m3u.urn: music.m3u
#	ccouch id "$<" > "$@"

#publish-m3u: music.m3u
#	publish -sector music music.m3u

managed_site_files = \
	public/music/music.mp3.m3u.urn public/music/music.ogg.m3u.urn public/music/music.txt \
	public/music/archive.org-urls.txt \
	public/lib/php/TOGoS/Nuke24/Resource/URNArchiveURLs.php

update-website: music.txt
	cp -a "music.txt" "archive.org-urls.txt" ../../sites/nuke24/public/music/
	cd ../../sites/nuke24 && make ${managed_site_files}

publish-website: update-website
	cd ../../sites/nuke24 && \
	git add ${managed_site_files} && \
	git commit -m "Update music list" && \
	gitpush

publish-mp3-files: music.m3u
	perl6 -e 'for (lines) { say "$$0" if /^ "#URN:" (.*) / }' music.m3u | sort -r | xargs publish -sector music

cache-music-files:
	grep -o -e 'urn:sha1:[A-Z0-9]\{32\}\|urn:bitprint:[A-Z0-9]\{32\}\.[A-Z0-9]\{39\}' music.txt | ccouch cache -sector music -link @-

# Everything you need to get it all uploaded!
publish: publish-website publish-mp3-files
